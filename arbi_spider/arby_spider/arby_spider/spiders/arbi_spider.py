# -*- coding: utf-8 -*-
import scrapy
import logging
import random
import scrapy_splash
from arby_spider.items import OddsParserItem
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait
from scrapy.http import HtmlResponse
import random
from arby_spider.logins.oddsportal import login_credentials
from bs4 import BeautifulSoup
from selenium.webdriver.remote.remote_connection import LOGGER
import re

username = login_credentials.keys()[random.randint(0,(len(login_credentials.keys())-1))]
password = login_credentials[username]

class ArbiSpiderOddsportal(scrapy.Spider):
    name = 'arbi_spider_oddsportal'
    login_url = 'http://www.oddsportal.com/login/'
    tennis_url = 'http://www.oddsportal.com/tennis/'
    start_urls = [tennis_url,]
    site = 'http://www.oddsportal.com'
    driver=''
    headers = {
        'Host': 'www.oddsportal.com',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5',
        'Accept-Encoding': 'gzip, deflate',
        'Referer': 'http://www.oddsportal.com/'}

    def __init__(self, name=None, **kwargs):
        if name is not None:
            self.name = name
        elif not getattr(self, 'name', None):
            raise ValueError("%s must have a name" % type(self).__name__)
        self.__dict__.update(kwargs)
        if not hasattr(self, 'start_urls'):
            self.start_urls = []
        self.options = Options()
        self.options.add_argument('--headless')
        self.options.add_argument('--disable-gpu')
        self.options.add_argument('--no-sandbox')
        self.options.add_argument("--disable-setuid-sandbox")
        self.options.add_argument('user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36')
        self.driver = webdriver.Chrome(chrome_options=self.options)
        self.driver.get(self.login_url)
        username_el = self.driver.find_element_by_id("login-username1")
        password_el = self.driver.find_element_by_id("login-password1")
        password_el.send_keys(password)
        username_el.send_keys(username)
        self.driver.find_elements_by_name("login-submit")[1].click()

    def start_requests(self):
        return [scrapy.Request(url=url,
                    callback=self.get_tournament_urls,
                    headers=self.headers) for url in self.start_urls]

    def get_tournament_urls(self,response):
        tournament_urls = response.xpath("//tr/td/a/@href").extract()
        for url in tournament_urls:
            if 'tennis' in url:
                yield scrapy.Request(url=self.site+url, callback=self.get_match_urls,headers=self.headers)

    def get_match_urls(self, response):
        match_urls = response.xpath("//table[@id='tournamentTable']/tbody/tr/td/a/@href").extract()
        for url in match_urls:
            if 'tennis' in url:
                yield scrapy.Request(url=self.site+url, callback=self.parse_match_urls,headers=self.headers,meta={'match':1})

    def from_us(self,numb):
        if int(numb)>0:
            return (float((numb/100)+1))
        else:
            numb=abs(numb)
            return (float((100/numb)+1))
    def from_frac(self, frac):
        no1=int(frac.split('/')[0])
        no2=int(frac.split('/')[1])
        return (float((no1/no2)+1))

    def check_koef(self,player1,player2):
        bet1 = 100
        res = []
        for bookie1 in player1.keys():
            for bookie2 in player2.keys():
                koef1 = float(player1[bookie1])
                koef2 = float(player2[bookie2])
                bet2 = koef1*bet1/koef2
                win = koef1*bet1
                bet = bet1 + bet2
                if win>bet:
                    diff = win-bet
                    perc = (diff/bet)*100
                    winning_pair = {}
                    winning_pair['percentage'] = perc
                    winning_pair['bookie1odds']=koef1
                    winning_pair['bookie2odds']=koef2
                    winning_pair['bookie1name']=bookie1
                    winning_pair['bookie2name']=bookie2
                    res.append(winning_pair)
        return res

    def parse_match_urls(self,response):
        self.driver.get(response.url)
        soup = BeautifulSoup(self.driver.page_source,'lxml')
        data_table = soup.find('div',{'id':'event-status'})
        if len(data_table)==0:
            data_table = soup.find('div',{'id':'col-content'})
            time = data_table.find('p',{'class':re.compile('date')}).getText()
            both_names = data_table.find('h1')
            both_names = str(both_names)
            tournament =' '.join(response.url.split('/')[5].split('-')).upper()
            both_names = both_names[4:len(both_names)-5]
            player1name = both_names.split('. -')[0]
            player2name = both_names.split('. -')[1]
            player1d = {}
            player2d = {}
            for tr in data_table.find_all_next('tr', class_='lo odd'):
                bookie=tr.find('a')['href'].split('/')[2]
                koef=tr.find('td',{'class':re.compile('right')})
                for a in koef:
                    check_deact = []
                    try:
                        check_deact = a['class']
                    except:
                        pass
                    check_deact = 'deactivateOdd' in check_deact
                    if not check_deact:
                        if '.' in a.getText():              #depending on wich odd is dropping
                            player1d[bookie] = float(a.getText())
                            player2d[bookie] = float(a.findNext('td').getText())
                        elif '/' in a.getText():
                            player1d[bookie] = self.from_frac(a.getText())
                            player2d[bookie] = self.from_frac(a.findNext('td').getText())
                        elif '+' in a.getText() or '-' in a.getText():
                            player1d[bookie] = self.from_us(float(a.getText().strip('+')))
                            player2d[bookie] = self.from_us(float(a.findNext('td').getText().strip('+')))
            for tr in data_table.find_all_next('tr', class_='lo even'):
                bookie=tr.find('a')['href'].split('/')[2]
                koef=tr.find('td',{'class':re.compile('right')})
                for a in koef:
                    check_deact = []
                    try:
                        check_deact = a['class']
                    except:
                        pass
                    check_deact = 'deactivateOdd' in check_deact
                    if not check_deact:
                        if '.' in a.getText():
                            player1d[bookie] = float(a.getText())
                            player2d[bookie] = float(a.findNext('td').getText())
                        elif '/' in a.getText():
                            player1d[bookie] = self.from_frac(a.getText())
                            player2d[bookie] = self.from_frac(a.findNext('td').getText())
                        elif '+' in a.getText() or '-' in a.getText():
                            player1d[bookie] = self.from_us(float(a.getText().strip('+')))
                            player2d[bookie] = self.from_us(float(a.findNext('td').getText().strip('+')))
            res = self.check_koef(player1d,player2d)
            if res:
                for a in res:
                    item=OddsParserItem()
                    item['player1name'] = player1name
                    item['player2name'] = player2name
                    item['player1odds'] = str(a['bookie1odds'])
                    item['player2odds'] = str(a['bookie2odds'])
                    item['player1bookie']=a['bookie1name']
                    item['player2bookie']=a['bookie2name']
                    item['matchtime'] = time
                    item['tournament'] = tournament
                    item['percentage'] = a['percentage']
                    yield item


