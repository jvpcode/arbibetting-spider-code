# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ArbySpiderItem(scrapy.Item):
    pass

class OddsParserItem(scrapy.Item):
    player1odds = scrapy.Field()
    player2odds = scrapy.Field()
    player1name = scrapy.Field()
    player2name = scrapy.Field()
    player1odds = scrapy.Field()
    player2odds = scrapy.Field()
    player1bookie = scrapy.Field()
    player2bookie = scrapy.Field()
    matchtime = scrapy.Field()
    tournament = scrapy.Field()
    percentage = scrapy.Field()


