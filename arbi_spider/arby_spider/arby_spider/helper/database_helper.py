import sqlalchemy
from sqlalchemy import Table, MetaData
from sqlalchemy import exc
from sqlalchemy import text

class DatabaseHelper(object):
    def connection(self):
        arby_engine_url =  "Here were the credentials for database so i removed them for this demo"
        arby_engine = sqlalchemy.create_engine(arby_engine_url)
        c = arby_engine.connect()
        meta = MetaData()
        arbibets_bet_ = Table('arbibets_bet', meta, autoload=True, autoload_with=arby_engine)
        return c, arbibets_bet_

    def store_bet_data(self,c,items):
        sql = """SELECT * from bookie_data"""
        res = c.execute(sql)
        bookies = [dict(item) for item in res]
        list_of_bookies = [str(bookie['name']) for bookie in bookies]
        reselect = 0
        for item in items:
            if str(item['player1bookie']) not in list_of_bookies:
                reselect=1
                esql=""" INSERT INTO bookie_data (name)
                        VALUES (:bookie_name)"""
                res = c.execute(text(esql),bookie_name=item['player1bookie'])
                list_of_bookies.append(item['player1bookie'])
            if str(item['player2bookie']) not in list_of_bookies:
                reselect = 1
                res = c.execute(text(esql),bookie_name=item['player2bookie'])
                list_of_bookies.append(item['player1bookie'])
        if reselect==1:
            res = c.execute(sql)
            bookies = [dict(item) for item in res]
        formatted_bets=[]
        sql = """SELECT * FROM bookie_bets WHERE bookie1_id=:bookie1_id and bookie2_id=:bookie2_id and date=:date"""
        for item in items:
            main_item={}
            main_item['date'] = item['matchtime']
            main_item['bookie1_odd'] = item['player1odds']
            main_item['bookie2_odd'] = item['player2odds']
            for bookie in bookies:
                if bookie['name']==item['player1bookie']:
                    main_item['bookie1_id']=bookie['bookie_id']
                if bookie['name']==item['player2bookie']:
                    main_item['bookie2_id']=bookie['bookie_id']
            formatted_bets.append(main_item)
        insert_sql = """INSERT INTO bookie_bets (bookie1_id, bookie2_id,bookie1_odd,bookie2_odd,date)
                        values (:bookie1_id,:bookie2_id,:bookie1_odd,:bookie2_odd,:date)
                        """
        for bet in formatted_bets:
            print(bet)
            res =c.execute(text(sql),bookie1_id=bet['bookie1_id'],bookie2_id=bet['bookie2_id'],date=bet['date'])
            result=[dict(item) for item in res]
            if len(result)==0:
                res = c.execute(text(insert_sql),bookie1_id=bet['bookie1_id'],
                    bookie2_id=bet['bookie2_id'],
                    bookie1_odd=bet['bookie1_odd'],
                    bookie2_odd=bet['bookie2_odd'],
                    date=bet['date'],
                    )


    def add_to_database(self, c,arbibets_bet_,items):
        sql = "DELETE FROM arbibets_bet where player1name is not null"
        try:
            res = c.execute(sql)
        except Exception as e:
            spider.logger.exception(e)
        items_ready = [item for item in items]
        try:
            for item in items_ready:
                sql = """INSERT INTO arbibets_bet (percentage, player2name, player2odds, player1name, player1odds, player1bookie, player2bookie,tournament, matchtime) 
                             VALUES ("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", '%s')""" %(item['percentage'],item['player2name'],item['player2odds'],item['player1name'],item['player1odds'],item['player1bookie'],item['player2bookie'],item['tournament'],item['matchtime'])

                res = c.execute(sql)
            self.items = []
            self.store_bet_data(c,items_ready)
            return 1 
        except exc.CompileError as e:
            spider.logger.exception(e)
            raise


