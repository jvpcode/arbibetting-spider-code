# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy import signals
from arby_spider.helper.database_helper import DatabaseHelper
import datetime
import pytz

class ArbySpiderPipeline(object):
    items = []
    database_helper = DatabaseHelper()
    connection,arbibets_bet_ = database_helper.connection()
    
    def real_time(self,parsed):
        months = {'Jan':1,'Feb':2,'Mar':3,'Apr':4,'May':5,'Jun':6,'Jul':7,'Aug':8,'Sep':9,'Oct':10,'Nov':11,'Dec':12}
        date = parsed.split(',')[1]
        hour = parsed.split(',')[2]
        minute = int(hour.split(':')[1])
        hour = int(hour.split(':')[0])
        month = int(months[date.split(' ')[2]])
        day = int(date.split(' ')[1])
        year = int(date.split(' ')[4])
        t = (datetime.datetime(year, month, day, hour, minute))
#        t = t - datetime.timedelta(hours = 2)
#        s = pytz.timezone('UTC').localize(t)
        return t

    def process_item(self, item, spider):
        item['matchtime'] = self.real_time(item['matchtime'])
        self.items.append(item)
        return item

    @classmethod
    def from_crawler(cls, crawler):
        temp = cls()
        crawler.signals.connect(temp.spider_closed, signal=signals.spider_closed)
        return temp

    def spider_closed(self, reason):
        if reason!='shutdown' and reason!='canceled':
            self.database_helper.add_to_database(self.connection, self.arbibets_bet_,self.items)
        self.connection.close()
